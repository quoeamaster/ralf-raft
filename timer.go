// Ralf - Raft Consensus Protocol implementation
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ralfraft

import (
	"time"
)

type StopWatch struct {
	start time.Time
}

// NewStopWatch - return an instance of [StopWatch]
func NewStopWatch() (s *StopWatch) {
	s = new(StopWatch)
	return
}

// Start - record the start time for the stopwatch.
// 	Start() should be called before running another round of time tracking
//		or else the previous start-time would be involved in the calculation.
func (s *StopWatch) Start() {
	s.start = time.Now()
}

// Done - return the diff between NOW and the starting time.
func (s *StopWatch) Done() int64 {
	diffTime := time.Since(s.start)

	// [fact] 9000000000 nanoseconds = 9 seconds (10^9)
	return diffTime.Nanoseconds()
}
