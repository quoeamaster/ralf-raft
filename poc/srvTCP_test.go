// Ralf - Raft Consensus Protocol implementation
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package poc

import (
	"bytes"
	"fmt"
	"net"
	"strings"
	"sync"
	"testing"
	"time"

	rr "gitlab.com/quoeamaster/ralf-raft"
)

const (
	tcpURL  = "localhost"
	tcpPort = 1688

	buffSize = 10
)

func Test_srvTCP_connectivity(t *testing.T) {
	t.Logf("\n** Test_srvTCP_connectivity **\n")
	// track the timing
	watch := rr.NewStopWatch()
	watch.Start()

	srv := NewSrcTCP(tcpPort, tcpURL)
	if e := srv.Connect(); nil != e {
		t.Fatalf("[1] %v", e)
	}
	defer srv.Close()

	// run some request(s)
	// convert these request(s) to waitgroup instead - simulate multi-threading behaviour
	var wgrp sync.WaitGroup
	for idx := 0; idx < 5; idx++ {
		wgrp.Add(1)
		go func() {
			if e := doTCPRequest(); nil != e {
				panic(fmt.Sprintf("[2] %v", e))
			}
			wgrp.Done()
		}()
	}
	wgrp.Wait()

	t.Logf("[3] before exit\n")
	t.Logf("time elapsed in nanoseconds - %v\n", watch.Done())
}

// TODO add a test for stress / load test
// echo a payload of 1Mb, 5Mb for 10,000 times each

// doTCPRequest - helper method to create a TCP request to the echo-server
func doTCPRequest() (e error) {
	conn, e := net.Dial("tcp", fmt.Sprintf("%v:%v", tcpURL, tcpPort))
	if nil != e {
		return
	}
	// write the echo message to server
	_, e = conn.Write([]byte(fmt.Sprintf(`{ "ts": "%v", "msg": "hello world" }%v`, time.Now().UTC().String(), SymbolEOL)))
	if nil != e {
		return
	}
	// read the respond
	bData := make([]byte, buffSize)
	var bRead bytes.Buffer
	for {
		// make sure a timeout is added since tcp has no idea or indicator when to "stop" a read...
		// [lesson] could also add a delimiter as indicator...
		conn.SetReadDeadline(time.Now().Add(time.Second))
		n, e := conn.Read(bData)
		if nil != e {
			if !strings.Contains(e.Error(), "i/o timeout") {
				fmt.Printf("[c-deb] non i/o timeout error: %vn", e)
				return e
			}
		}
		if n > 0 {
			bRead.Write(bData[0:n])
			// exit?
			if n < buffSize {
				break
			}
		} else {
			break
		}
	}
	/* // [another approach] - 1 bulk load through reader
	bData := make([]byte, 10240)
	n, e := bufio.NewReader(conn).Read(bData)
	if nil != e {
		fmt.Printf("[c-deb] failed to read from channel: %v\n", e)
		return
	}
	fmt.Printf("[c-deb] data read: %v\n", string(bData[0:n]))
	*/
	fmt.Printf("[c-deb] data read:     %v\n", bRead.String())

	return
}
