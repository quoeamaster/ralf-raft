// Ralf - Raft Consensus Protocol implementation
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package poc

import (
	"bytes"
	"fmt"
	"net"
	"strings"
	"sync"
)

const (
	SymbolEOL string = "__eol__"
	BuffSize  int    = 10240
)

// SrvTCP the struct server
type SrvTCP struct {
	Port int
	URL  string

	pTcpConn         net.Listener
	shouldStopAccept bool

	lock sync.Mutex
}

// NewSrcTCP - create a new instance of SrvTCP
func NewSrcTCP(port int, url string) (instance *SrvTCP) {
	instance = new(SrvTCP)
	instance.Port = port
	instance.URL = url
	instance.shouldStopAccept = false
	instance.lock = sync.Mutex{}
	return
}

func (s *SrvTCP) Connect() (e error) {
	if nil == s.pTcpConn {
		s.pTcpConn, e = net.Listen("tcp", fmt.Sprintf("%v:%v", s.URL, s.Port))
		if e != nil {
			// might induce another error though, but will shadow this one
			s.Close()
			return
		}
		// run as a routine / thread
		go func() {
			for {
				// [lesson] locking the access to shouldStopAccept
				s.lock.Lock()
				if !s.shouldStopAccept {
					// [lesson] quickly unlock the shouldStopAccept
					s.lock.Unlock()

					if conn, e2 := s.pTcpConn.Accept(); e2 != nil {
						e = fmt.Errorf("[Connect] failed to connect and accept incoming requests: %v", e2)
						break
					} else {
						// open another routine per request
						go s.Request(conn)
					}
				} // end -- if shouldStopAccept check
			} // end -- for (true)
		}() // end -- routine declaration
	} // end -- if pTcpConn is to be SET
	return
}

// io.Closer interface implementation
func (s *SrvTCP) Close() (e error) {
	fmt.Printf("[deb] close server\n")
	if nil != s.pTcpConn {
		// final catch on this
		s.lock.Lock()
		if !s.shouldStopAccept {
			s.shouldStopAccept = true
		}
		s.lock.Unlock()
		e = s.pTcpConn.Close()
	}
	return
}

// Request - handler for incoming request, might be acting just a router here - routing
// the request to the correct handler.
func (s *SrvTCP) Request(conn net.Conn) (e error) {
	// echo server...
	// [obsolete] var bRead []byte // slice (auto expand)
	bData := make([]byte, BuffSize) // array (fixed size)
	var bRead bytes.Buffer
	// [lesson] read until met "SymbolEOL"
	// [approach] another approach is by setting a setReadTimeout(time.Second) but again,
	//		what happens if there were really a 1 second delay due to network? Then that delay
	//		would trigger a timeout and stop reading the stream / socket / connection.
	for {
		n, e2 := conn.Read(bData)
		if nil != e2 {
			e = fmt.Errorf("[Request] failed to read from the connection: %v", e2)
			break
		} else if n > 0 {
			bRead.Write(bData[0:n])
			// [debug]
			//fmt.Printf("[deb] in-progress %v \n", bRead.String())
			// is last few sequence related to the eol?
			if bRead.Len() > len(SymbolEOL) && strings.Index(bRead.String(), SymbolEOL) == bRead.Len()-len(SymbolEOL) {
				break
			}
		}
	}
	// any error occured?
	if nil != e {
		return
	}
	// actual contents to be echoed
	fmt.Printf("[deb] data to be echo: %v\n", bRead.String())
	_, e = conn.Write(bRead.Bytes())
	return
}

/* [obsolete] redundant and logic run by Close() instead
func (s *SrvTCP) StopAcceptConnections() {
	s.shouldStopAccept = true
}
*/
