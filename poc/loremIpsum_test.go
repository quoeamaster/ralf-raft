// Ralf - Raft Consensus Protocol implementation
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package poc

import (
	"strings"
	"testing"
	"time"
)

func Test_loremIpsum_sentence(t *testing.T) {
	t.Logf("\n** Test_loremIpsum_sentence **\n")

	s := NewLoremIpsumGenerator(time.Now().UnixNano())
	var payload string

	// test on sentence generation with total word length within 3 to 12-1.
	payload = s.Sentence(3, 12)
	//t.Logf("(3, 12): %v\n", payload)
	if !isLoremIpsumSentenceValid(payload, 3, 12) {
		t.Errorf("(3, 12) failed to generate the correct sentence structure: %v\n", payload)
	}
	payload = s.Sentence(12, 3)
	//t.Logf("(12, 3): %v\n", payload)
	if payload != "" {
		t.Errorf("(12, 3) expect an empty string instead of: %v\n", payload)
	}
}

func Test_loremIpsum_paragraph(t *testing.T) {
	t.Logf("\n** Test_loremIpsum_paragraph **\n")

	s := NewLoremIpsumGenerator(time.Now().UnixNano())
	var payload string

	// test on sentence generation with total word length within 3 to 12-1.
	payload = s.Paragraph(3, 9, 2, 8)
	//t.Logf("(3, 9, 2, 8): %v\n", payload)
	if !isLoremIpsumParagraphValid(payload, 2, 8, 3, 9) {
		t.Errorf("(3, 9, 2, 8) failed to generate the correct paragraph structure: %v\n", payload)
	}
	payload = s.Paragraph(12, 3, 2, 5)
	//t.Logf("(12, 3, 2, 5): %v\n", payload)
	if payload != "" {
		t.Errorf("(12, 3, 2, 5) expect an empty string instead of: %v\n", payload)
	}
	payload = s.Paragraph(3, 12, 5, 1)
	//t.Logf("(3, 12, 5, 1): %v\n", payload)
	if payload != "" {
		t.Errorf("(3, 12, 5, 1) expect an empty string instead of: %v\n", payload)
	}
}

func Test_loremIpsum_size(t *testing.T) {
	t.Logf("\n** Test_loremIpsum_size **\n")

	s := NewLoremIpsumGenerator(time.Now().UnixNano())
	var payload = ""

	payload = s.Size(9999, UnitBytes)
	//t.Logf("(9999 size): %v\n", payload)
	if len(payload) != 9999 {
		t.Errorf("[9999] expect length is 9999, actual %v\n", len(payload))
	}
	payload = s.Size(2, UnitKiloBytes)
	//t.Logf("2kb: %v\n", payload)
	if len(payload) != (2 * 1024) {
		t.Errorf("[2kb] expect length is 9999, actual %v\n", len(payload))
	}
	payload = s.Size(1, UnitMegaBytes)
	//t.Logf("1mb: %v\n", payload)
	if len(payload) != (1024 * 1024) {
		t.Errorf("[1mb] expect length is 9999, actual %v\n", len(payload))
	}
}

// isLoremIpsumSentenceValid - helper to validate whether
// the number of words in the given sentence is within the range or not.
func isLoremIpsumSentenceValid(sentence string, min, max int) bool {
	words := strings.Split(sentence, " ")
	numOfWords := len(words)

	return numOfWords >= min && numOfWords < max
}

// isLoremIpsumParagraphValid - helper to valid whether the paragraph meets the requirements
func isLoremIpsumParagraphValid(paragraph string, min, max, minWords, maxWords int) bool {
	sentences := strings.Split(paragraph, ".")
	if sentences[len(sentences)-1] == "" {
		sentences = sentences[0 : len(sentences)-1]
	}
	numOfSentences := len(sentences)
	return numOfSentences >= min && numOfSentences < max
	/*
		valid := false
		// sentence check
		if numOfSentences >= min && numOfSentences < max {
			valid = true
		}
		// sentence check
		if true == valid {
			for i := 0; i < numOfSentences; i++ {
				if sentenceValid := isLoremIpsumSentenceValid(sentences[i], minWords, maxWords); !sentenceValid {
					valid = false
					break
				}
			}
		}
		return valid*/
}
