// Ralf - Raft Consensus Protocol implementation
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// Test_customServeMux
// objective:
//	- test whether a custom ServeMux works exactly like the defaultServeMux instance provided in http package
// - test the ability to add handlers or handler-functions
// - using the httptest server to run the test on behalf
//
// reference:
// - https://golang.cafe/blog/golang-httptest-example.html
//
// conclusions:
//	- GREAT, worked as expected
func Test_customServeMux(t *testing.T) {
	t.Logf("\r\n** Test_customServeMux **\n")

	// create a ServeMux
	pServer := http.NewServeMux()
	// add handler(s)
	pServer.HandleFunc("/help", func(w http.ResponseWriter, r *http.Request) {
		bResponse, _ := json.Marshal(`{ 
			"message": "help is under-development..." 
		}`)
		w.WriteHeader(http.StatusOK)
		w.Write(bResponse)
	})
	pServer.HandleFunc("/stats", func(w http.ResponseWriter, r *http.Request) {
		// create response in a Map way
		mPayload := make(map[string]interface{})
		mPayload["stats"] = "STATS is also under-development..."
		mPayload["budget"] = 2000
		bResponse, _ := json.Marshal(mPayload)
		w.WriteHeader(http.StatusOK)
		w.Write(bResponse)
	})

	// create a test-server wrapping up the actual server above
	pTestServer := httptest.NewServer(pServer)
	t.Logf("- server url: %v\n", pTestServer.URL)
	defer pTestServer.Close()

	// create client / http-request
	if response, err := http.Get(fmt.Sprintf("%v/help", pTestServer.URL)); err != nil {
		t.Errorf("failed to [/help] %v", err)
	} else {
		bData, _ := ioutil.ReadAll(response.Body)
		line := string(bData)
		t.Logf("[deb] help -> %v\n", line)
		if strings.Contains(line, "help is under-development") {
			t.Logf("- cool~ help is NORMAL\n")
		} else {
			t.Errorf("- damn~ help is not achieved, %v\n", line)
		}
	}

	if response, err := http.Get(fmt.Sprintf("%v/stats", pTestServer.URL)); err != nil {
		t.Errorf("failed to [/help] %v", err)
	} else {
		bData, _ := ioutil.ReadAll(response.Body)
		line := string(bData)
		t.Logf("[deb] stats -> %v\n", line)
		// unmarshal back to a map
		mPayload := make(map[string]interface{})
		json.Unmarshal(bData, &mPayload)
		t.Logf("[deb] help [in map] %v; budget: %v %t", mPayload, mPayload["budget"], mPayload["budget"])

		if strings.Contains(line, "STATS is also under-development") {
			t.Logf("- cool~ stats is NORMAL\n")
		} else {
			t.Errorf("- damn~ stats is not achieved, %v\n", line)
		}
	}
}

/*
 *		idea: add annotations on the function (e.g. test-func-name)
 *		annotations would affect how this function behaves
 */
